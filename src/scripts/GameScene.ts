import * as PIXI from 'pixi.js';

import { MainApp } from './app';
import Reel from './Reel';
import Server from './Server';
import TexturesManager from './TexturesLoader';

// Utilities, constant
import {
    GameConfig,
    GameState,
    getMatchedResult,
    MatchResult,
    SPIN_TEXT_COLOR,
    symbolText
} from './utilities';

export class GameScene extends PIXI.Container {
    constructor(server: Server) {
        super();

        /**
         * Register spin data responded event handler
         */
        this._server = server;
        this._server.registerDataRespondEvent(this._onSpinDataResponded.bind(this));

        /**
         * Added onUpdate function to PIXI Ticker so it will be called every frame
         */
        MainApp.inst.app.ticker.add(this._onUpdate, this);

        this._state = GameState.INIT;

        /**
         * Ask PIXI Loader to load needed resources
         */
        TexturesManager.inst || new TexturesManager();
        TexturesManager.inst
            .addTexture('logo', 'images/logo.png')
            .addTexture('highlight', 'images/highlight.png')
            .addTexture('1', 'images/symbol_1.png')
            .addTexture('2', 'images/symbol_2.png')
            .addTexture('3', 'images/symbol_3.png')
            .addTexture('4', 'images/symbol_4.png')
            .addTexture('5', 'images/symbol_5.png')
            .addTexture('6', 'images/symbol_6.png')
            .addTexture('7', 'images/symbol_7.png')
            .addTexture('8', 'images/symbol_8.png')
            .addTexture('K', 'images/symbol_K.png')
            .addTexture('1_blur', 'images/symbol_1_blur.png')
            .addTexture('2_blur', 'images/symbol_2_blur.png')
            .addTexture('3_blur', 'images/symbol_3_blur.png')
            .addTexture('4_blur', 'images/symbol_4_blur.png')
            .addTexture('5_blur', 'images/symbol_5_blur.png')
            .addTexture('6_blur', 'images/symbol_6_blur.png')
            .addTexture('7_blur', 'images/symbol_7_blur.png')
            .addTexture('8_blur', 'images/symbol_8_blur.png')
            .addTexture('K_blur', 'images/symbol_K_blur.png')
            .loadTextures(this._onAssetsLoaded.bind(this));
    }

    // Game state
    private _state: GameState;

    // Mock server
    private _server: Server;

    // Logo and Spin button
    private _logoSprite: PIXI.Sprite;
    private _spinText: PIXI.Text;

    // Total spin time, to limit min spin time.
    private _spinTime: number = 0;
    private _result: string[][] = null;
    private _matchedResult: MatchResult;

    // Reels, for quick access instead this.child
    private _reels: Reel[] = [];

    public init(): void {

        // Create logo.
        this.addChild(this._logoSprite);
        this._logoSprite.position.set(720 / 2, 100);
        this._logoSprite.anchor.set(0.5);
        this._logoSprite.scale.set(0.5);

        // Create spin button
        const style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 36,
            fontWeight: 'bold',
            fill: SPIN_TEXT_COLOR.active, // gradient
            stroke: '#4a1850',
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
            wordWrap: true,
            wordWrapWidth: 440
        });
        this._spinText = new PIXI.Text('Start Spin', style);
        this._spinText.x = 720 / 2 - this._spinText.width / 2;
        this._spinText.y =
            MainApp.inst.app.screen.height - 100 + Math.round((100 - this._spinText.height) / 2);
        this.addChild(this._spinText);

        // Inactive until loading successfully
        this._spinText.interactive = false;
        this._spinText.buttonMode = false;
        this._spinText.addListener('pointerdown', this._startSpin.bind(this));
        
        this._createBoard();

        this._state = GameState.READY;
        this._toggleSpinButton(true);
    }

    /**
     * Update event
     * @param dtScalar - 
     */
    private _onUpdate(dtScalar: number) {

        const frameTime = dtScalar / PIXI.settings.TARGET_FPMS / 1000;

        if (this._state !== GameState.INIT) {
            this._logoSprite.rotation += 0.01;
            this._reels.forEach((reel: Reel) => reel.onUpdate(frameTime));
        }

        // Keep spinning until finish
        if (this._state === GameState.SPINNING || this._state === GameState.RESULT_RECEIVED) {
            this._spinTime += frameTime;

            // If result received, wait until over min spin time.
            if (this._state === GameState.RESULT_RECEIVED) {
                if (this._spinTime >= GameConfig.MIN_SPIN_TIME) {
                    this._spinTime = 0;
    
                    // Start show result
                    this._applyResultToReels(this._result, this._onShowMatchedResult.bind(this));
                }
            }

        }

        if (this._state === GameState.SHOW_RESULT) {
            this._matchedResult.reelMatched.forEach((reelIdx: number) => {
                this._reels[reelIdx].setMatchedReel();
            });
            this._matchedResult.rowMatched.forEach(rowIdx => {
                this._reels.forEach((reel: Reel) => reel.setMatchSymbol(rowIdx));
            });

            this._state = GameState.READY;
        }
    }

    /**
     * Toggle Spin button status
     * @param active - is active spin button
     */
    private _toggleSpinButton(active: boolean): void {
        this._spinText.interactive = active;
        this._spinText.buttonMode = active;
        this._spinText.style.fill = active? SPIN_TEXT_COLOR.active : SPIN_TEXT_COLOR.disabled;
    }

    /**
     * Create main board and all needed to play
     */
    private _createBoard(): void {
        const boardContainer = this.addChild(new PIXI.Container());
        const boardHeight = GameConfig.SYMBOL_HEIGHT * GameConfig.NUMBER_OF_ROWS;
        const boardWidth = GameConfig.SYMBOL_WIDTH * GameConfig.NUMBER_OF_REELS;

        // Start of board. Move out PixiJS logo
        boardContainer.position = new PIXI.Point(75, 300);

        // Mask, hide over result
        const graphics = new PIXI.Graphics();
        graphics.beginFill();
        graphics.drawRect(0, 240, boardWidth, boardHeight);
        graphics.endFill();
        boardContainer.mask = graphics;

        // Create reels.
        for (let reelIdx = 0; reelIdx < GameConfig.NUMBER_OF_REELS; reelIdx++) {
            const reelContainer = new Reel();

            reelContainer.position = new PIXI.Point(reelIdx * GameConfig.SYMBOL_WIDTH, 0);
            reelContainer.init(GameConfig.NUMBER_OF_ROWS + 1);

            boardContainer.addChild(reelContainer);
            this._reels.push(reelContainer);
        }
    }

    /**
     * Manual start all reels spinning
     */
    private _startReelSpin(): void {
        this._reels.forEach((reel: Reel, idx: number) => {
            setTimeout(() => {
                reel.startSpin();
            }, idx * GameConfig.SPIN_STEP_TIME);
        });
    }

    /**
     * Apply received result and stop spin
     */
    private _applyResultToReels(data: string[][], callback: Function): void {
        this._reels.forEach((reel: Reel, idx: number) => {
            const result = data[idx];
            setTimeout(() => {

                // After stop last reel
                if (idx === GameConfig.NUMBER_OF_REELS - 1) {
                    reel.setSpinResult(result, callback);
                } else {
                    reel.setSpinResult(result)
                }

            }, idx * GameConfig.SPIN_STEP_TIME);
        });
    }

    private _onShowMatchedResult(): void {
        this._matchedResult = getMatchedResult(this._result);
        this._state = GameState.SHOW_RESULT;
        this._toggleSpinButton(true);
    }

    /**
     * Serialize data. Update this if need to change load rule
     * @param data - received data from server
     * @returns - serialized data
     */
    private serializeResult(data: string[]): string[][] {
        let reels = GameConfig.NUMBER_OF_REELS;
        const result = [];

        while (reels--) {
            let rows = GameConfig.NUMBER_OF_ROWS;
            let rowResult = [];
            while (rows--) {
                rowResult.push(data.shift());
            }
            result.push(rowResult);
        }
        return result;
    }

    /**
     * ========================= Events ================================
     */
    private _startSpin(): void {
        console.log(` >>> start spin`);
        this._server.requestSpinData();

        // Disable Spin
        this._toggleSpinButton(false);
        this._startReelSpin();
        this._state = GameState.SPINNING;
    }

    private _onSpinDataResponded(data: string[]): void {
        console.log(` >>> received: ${data}`);
        console.log(data.map(i => symbolText[i]).join(','));

        this._result = this.serializeResult(data);
        this._state = GameState.RESULT_RECEIVED;
    }

    private _onAssetsLoaded(textureMgr: TexturesManager): void {
        
        this._logoSprite = new PIXI.Sprite(textureMgr.getTexture('logo'));
        
        this.init();
    }
    /**
     * ========================= Events end ============================
     */
}
