import * as PIXI from 'pixi.js';

import TexturesManager from './TexturesLoader';

import { GameConfig } from './utilities';


export default class Symbol extends PIXI.Container {

    private _symbolTexture: PIXI.Sprite;
    private _additionalTexture: PIXI.Sprite;

    constructor(texture? : PIXI.Texture, position?: PIXI.Point) {
        super();

        this._symbolTexture = new PIXI.Sprite(texture || TexturesManager.inst.getRandomSymbolTexture());
        this._additionalTexture = new PIXI.Sprite(TexturesManager.inst.getTexture('highlight'));

        this.addChild(this._symbolTexture);
        this.addChild(this._additionalTexture);
        if (position) {
            this.position = position;
        }

        // For align, effect
        this._symbolTexture.anchor.set(0.5);
        this._additionalTexture.anchor.set(0.5, 0.5);
        this._additionalTexture.alpha = 0.5;
        this._additionalTexture.renderable = false;
        this._additionalTexture.scale = new PIXI.Point(0, 0);
    }

    public setRandomSymbol(isBlur: boolean = false): void {
        this._symbolTexture.texture = TexturesManager.inst.getRandomSymbolTexture(isBlur);
    }

    /**
     * Create a symbol (Sprite)
     * @param x - x position
     * @param y - y position
     * @returns - new Sprite (Symbol)
     */
    public setPosition(x: number, y: number): void {
        const pos = new PIXI.Point(x, y);
        this.position = pos;
    }

    public setTexture(fn: () => PIXI.Texture): void;
    public setTexture(texture: PIXI.Texture): void;
    public setTexture(...args: any[]): void {
        if (args[0] instanceof PIXI.Texture) {
            this._symbolTexture.texture = args[0];
        }
        if (args[0] instanceof Function) {
            this._symbolTexture.texture = args[0]();
        }
    }

    /**
     * Toggle highlight of symbol (Only use for show matched symbol)
     * @param isShowHighlight is show highlight or not
     */
    public toggleHighlight(isShowHighlight: boolean = false): void {
        this._additionalTexture.renderable = isShowHighlight;
        this._additionalTexture.scale = new PIXI.Point(0, 0);
    }

    /**
     * Update symbol
     * @param deltaTime frame time
     */
    public onUpdate(deltaTime: number): void {

        // Highlight effect if renderable
        if (this._additionalTexture.renderable) {
            this._additionalTexture.rotation += 0.01;

            if (this._additionalTexture.scale.x <= GameConfig.HIGHLIGH_MAX_SCALE) {
                const scale = this._additionalTexture.scale.x + 25 * deltaTime;
                this._additionalTexture.scale = new PIXI.Point(scale, scale);
            }
        }
    }

}
