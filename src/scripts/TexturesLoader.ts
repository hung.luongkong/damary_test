import * as PIXI from 'pixi.js';

import {
    GameConfig,
    symbolTypes
} from './utilities';


/**
 * TexturesManager use same as a PIXI.Loader
 * Is extended from PIXI.Loader, but custom for 
 * get texture, symbol, blur symbol, random symbol.
 */
export default class TexturesManager extends PIXI.Loader {
    public static inst: TexturesManager;

    constructor() {
        super();

        TexturesManager.inst = this;
    }

    private textures = {};

    /**
     * Add a resource texture for load
     * @param params - same with PIXI.Loader.add
     * @returns - textures manager
     */
    public addTexture(...params: any[]): TexturesManager {
        super.add(...params);
        return this;
    }

    /**
     * Load all texture in queue
     * @param callback - callback after resource loaded
     */
    public loadTextures(callback: Function): void {
        super.load(this._onAssetsLoaded(callback));
    }

    /**
     * Get a random texture for symbol
     * @param isBlur - is using blur texture or not
     * @returns symbol texture
     */
    public getRandomSymbolTexture(isBlur = false): PIXI.Texture {
        const textureType = symbolTypes[~~(Math.random() * GameConfig.NUMBER_OF_SYMBOL_TYPES)];
        const sourceTextures = isBlur ? `${textureType}_blur` : textureType;
        return this.getTexture(sourceTextures);
    }

    /**
     * Get a symbol texture (not allow blur)
     * @param textureId - id of loaded texture
     * @returns symbol texture
     */
    public getTexture(textureId: string): PIXI.Texture {
        return this.textures[textureId];
    }

    private _onAssetsLoaded =
        (callback: Function) =>
        (_: PIXI.Loader, resources: Partial<Record<string, PIXI.LoaderResource>>) => {

            // Save all texture name
            for (const [key, value] of Object.entries(resources)) {
                this.textures[key] = value.texture;
            }

            callback(this);
        };
}
