const symbolTypes = ['1', '2', '3', '4', '5', '6', '7', '8', 'K'];

// For human
const symbolText = {
    '1': 'A',
    '2': 'J',
    '3': 'Q',
    '4': 'K',
    '5': 'Day',
    '6': 'Chai',
    '7': 'Thap',
    '8': 'Cao',
    'K': 'Cuc',
};

class GameConfig {
    static readonly NUMBER_OF_REELS = 5;
    static readonly NUMBER_OF_ROWS = 3;
    static readonly SYMBOL_WIDTH = 140;
    static readonly SYMBOL_HEIGHT = 150;
    static readonly SPIN_STEP_TIME = 300; //ms
    static readonly NUMBER_OF_SYMBOL_TYPES = symbolTypes.length; // 9
    static readonly MIN_SPIN_TIME: number = 2; // s
    static readonly TEXTURE_RANDOM_TIME = 0.1; // s
    static readonly SPIN_SPEED = 4000;
    static readonly HIGHLIGH_MAX_SCALE = 1.1;
};

enum GameState {
    INIT,
    READY,
    SPINNING,
    RESULT_RECEIVED,
    SHOW_RESULT
};

const RULES = {
    sameInRow: true,
    sameInReel: true
};

type MatchResult = {
    reelMatched: number[],
    rowMatched: number[],
};

const SPIN_TEXT_COLOR = {
    active: ['#ffffff', '#00ff99'],
    disabled: ['#535963']
};

const isAllSame = (data: string[]): boolean => {
    return !data.filter(i => i !== data[0]).length;
};

// Transpose 2d array.
const transpose = (m: string[][]) => m[0].map((x,i) => m.map(x => x[i]));

/**
 * Check match symbol from default rule.
 * @param data response as 2d array
 * @returns - match result
 */
const getMatchedResult = (data: string[][]): MatchResult => {
    const matched: MatchResult = {
        reelMatched: [],
        rowMatched: []
    }

    if (RULES.sameInReel) {
        data.forEach((reel, idx) => {
            if (isAllSame(reel)) {
                matched.reelMatched.push(idx);
            }
        }); 
    }

    if (RULES.sameInRow) {
        const transposeData = transpose(data);
        transposeData.forEach((reel, idx) => {
            if (isAllSame(reel)) {
                matched.rowMatched.push(idx);
            }
        });  
    }
    
    return matched;
};

export {
    symbolText,
    symbolTypes,
    GameConfig,
    GameState,
    MatchResult,
    getMatchedResult,
    SPIN_TEXT_COLOR
}