import * as PIXI from 'pixi.js';
import Symbol from './Symbol';

import TexturesManager from './TexturesLoader';

import {
    GameConfig
} from './utilities';

//----------------------------------------------------------------
/**
 * 1 Reel have n symbol will need n + 1 symbol for spin effect.
 * Result will alway apply to symbol 1 -> n and stop spin will show
 * symbol 1 -> n.
 */
//----------------------------------------------------------------


enum ReelState {
    READY,
    SPINNING,
    READY_APPLY_RESULT,
    RESULT_APPLIED
}

export default class Reel extends PIXI.Container {
    constructor() {
        super();
    }

    private _state: ReelState;

    private _showResultCallback: Function = null;

    private _numOfSymbol: number = 0;
    private _deltaTime: number = 0;
    private _spinResult: string[] = null;

    // Quick access
    private _symbols: Symbol[] = [];
    private _lastSymbol: Symbol;

    /**
     * Create reel's symbol
     * @param numOfSymbol - number of symbol to create.
     */
    public init(numOfSymbol: number): void {
        this._numOfSymbol = numOfSymbol;

        for (let symbolIdx = 0; symbolIdx < numOfSymbol; symbolIdx++) {
            const symbol = new Symbol(null, new PIXI.Point(0, GameConfig.SYMBOL_HEIGHT * symbolIdx));
            this.addChild(symbol);

            this._symbols.push(symbol);
            this._lastSymbol = symbol;
        }

        this._state = ReelState.READY;
    }

    /**
     * Update
     * @param deltaTime - delta time of frame
     */
    public onUpdate(deltaTime: number): void {

        if (this._state === ReelState.SPINNING) {
            this._spinning(deltaTime);
        }
        
        // If have spin result and not apply yet
        if (this._state === ReelState.READY_APPLY_RESULT) {
            if (this._lastSymbol.y === -GameConfig.SYMBOL_HEIGHT) {
                this._onUpdateSpinResult();
            } else {
                this._spinning(deltaTime);
            }
        }

        this._symbols.forEach((symbol) => {
            symbol.onUpdate(deltaTime);
        });
    }

    /**
     * Manual trigger reel spin
     */
    public startSpin(): void {
        this._state = ReelState.SPINNING;
        this.setMatchedReel(false);
        // this.blurReel(4);
    }

    /**
     * Set spin result for a reel
     * @param result - list of symbol
     */
    public setSpinResult(result: string[], callback?: Function): void {
        this._state = ReelState.READY_APPLY_RESULT;

        // This will trigger set result in update fn
        this._spinResult = [...result];
        this._showResultCallback = callback;
    }

    /**
     * Set match/un-match status of reel
     * @param isMatched - is matched all symbol in reel
     */
    public setMatchedReel(isMatched: boolean = true): void {
        this._symbols.forEach((symbol: Symbol) => symbol.toggleHighlight(isMatched));
    }

    /**
     * Set single symbol matched
     * @param symbolIndex index of matched symbol
     */
    public setMatchSymbol(symbolIndex: number): void {
        this._symbols[symbolIndex] && this._symbols[symbolIndex].toggleHighlight(true);
    }

    /**
     * Spin reel and make spin effect
     * @param deltaTime frame time
     */
    private _spinning(deltaTime: number): void {
        this._deltaTime += deltaTime;

        // Make random symbol
        if (this._deltaTime >= GameConfig.TEXTURE_RANDOM_TIME) {
            this._deltaTime -= GameConfig.TEXTURE_RANDOM_TIME;
            this._generateRandomSymbol(true);
        }

        // Spin and loop effect
        const deltaY = deltaTime * GameConfig.SPIN_SPEED;
        this._symbols.forEach((symbol) => {
            symbol.y += deltaY;
            if (symbol.y > GameConfig.SYMBOL_HEIGHT * (this._numOfSymbol - 1)) {
                symbol.y = -GameConfig.SYMBOL_HEIGHT;
            }
        });
    }

    /**
     * Generate random symbol.
     * @param isUseBlur - is use blur texture or not.
     */
    private _generateRandomSymbol(isUseBlur: boolean): void {
        this._symbols.forEach(symbol => {
            symbol.setRandomSymbol(isUseBlur);
        });
    }

    // Update result based on response and clean cache
    private _onUpdateSpinResult() : void {
        this._symbols.forEach((symbol: Symbol, idx: number) => {
            symbol.setTexture(TexturesManager.inst.getTexture(this._spinResult[idx]))
        });
        this._state = ReelState.RESULT_APPLIED;

        this._showResultCallback && this._showResultCallback();

        this._showResultCallback = null;
        this._spinResult = null;
    }

    /**
     * Set reel blur
     * @param blur - blur effect ratio
     */
    private _blurReel(blur: number): void {
        const blurFilter = new PIXI.filters.BlurFilter(blur);
        this.filters = [blurFilter];
    }
}